/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newPackage;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Semaphore;

/**
 *
 * @author martamolinaperez
 */
public interface Constantes {
    public static final int MAX_ESCENAS_EN_ESPERA = 5;
    public static final int NUM_GENERADORES=7;

    

    
    public static Semaphore mainMutex = new Semaphore(1);
    public static Semaphore emptySemAltaPrioridad = new Semaphore(0);
    public static Semaphore emptySemBajaPrioridad = new Semaphore(0);
    public static Semaphore fillSemAltaPrioridad = new Semaphore(MAX_ESCENAS_EN_ESPERA);
    public static Semaphore fillSemBajaPrioridad=new Semaphore(MAX_ESCENAS_EN_ESPERA);
    public static Semaphore mutexAltaPrioridad = new Semaphore(1);
    public static Semaphore mutexBajaPrioridad = new Semaphore(1);
    public static Semaphore barrera = new Semaphore(Constantes.NUM_GENERADORES);
    
    public static final int NUM_RENDERIZADORES=7;
    public static final int VARIACION_NUM_ESCENAS = 2;
    public static final int MIN_NUM_ESCENAS = 1;
    public static final int VARIACION_TIEMPO = 10;
    public static final int MIN_TIEMPO_GENERACION = 3;
    public static final int TIEMPO_DURACION_ESCENA = 5;
    public static final int VARIACION_NUM_FOTOGRAMAS=3;
    public static final int MIN_NUM_FOTOGRAMAS=1;
    public static final int MIN_TIEMPO_FOTOGRAMA = 5;

    
    public ArrayList<Escena> listaAltaPrioridad = new ArrayList();
    public ArrayList<Escena> listaBajaPrioridad = new ArrayList();
    
     
}
