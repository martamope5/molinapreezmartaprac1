/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newPackage;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import static newPackage.Constantes.NUM_GENERADORES;
import static newPackage.Constantes.NUM_RENDERIZADORES;

/**
 *
 * @author martamolinaperez
 */
public class main {

   
      
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        // TODO code application logic here
        int numEscGeneradas=0;
        int timepoRenderizacion=0;
        
        
        
        ExecutorService ejecucion = Executors.newCachedThreadPool();
        
        ArrayList<GeneradorEscena> generadores = new ArrayList();
        ArrayList<RederizadorEscena> renderizadores = new ArrayList();
        ArrayList<Future<ArrayList<Escena>>> resultadoGeneradores = new ArrayList();
        ArrayList<Future<ArrayList<Escena>>> resultadoRenderizadores = new ArrayList();
        
        System.out.println("Creamos los renderizadores");
        for (int i = 0; i < NUM_RENDERIZADORES; i++) {
            
           RederizadorEscena renderizador = new RederizadorEscena();
           renderizadores.add(renderizador);
        }
        //creamos el finalizador para los generadores, pasandole por parametro el array de renderizadores.
        Finalizador fin = new Finalizador(renderizadores);
        
        System.out.println("Creamos los generadores y se ejecutan");
        for (int i = 0; i < NUM_GENERADORES; i++) {
            
            GeneradorEscena generador = new GeneradorEscena(fin);
            generadores.add(generador);
            Future<ArrayList<Escena>> result1 = ejecucion.submit(generador);
            resultadoGeneradores.add(result1);
        }
        System.out.println("numero generadores: "+generadores.size());
       
        
        System.out.println("Comienza la renderizacion...");
        
        for (int i = 0; i < NUM_RENDERIZADORES; i++) {
            
            Future<ArrayList<Escena>> result2 = ejecucion.submit(renderizadores.get(i));
            resultadoRenderizadores.add(result2);
            
        }
        
        System.out.println("numero renderizadores: "+renderizadores.size());
        
        
        
        System.out.println("Finaliza el marco ejecutor");

        
        ejecucion.shutdown();
        
         System.out.println("esperamos a que todos finalicen...");
        // esperamos a que finalicen todos
        ejecucion.awaitTermination(1, TimeUnit.DAYS);

         System.out.println("TERMINADO.");
        
        for(int i =0; i<resultadoGeneradores.size();i++){
            numEscGeneradas = numEscGeneradas + resultadoGeneradores.get(i).get().size();
        }
        System.out.println("El numero total de escenas generadas es : "+numEscGeneradas);

        //Por cada renderizador
        for(int i = 0; i < resultadoRenderizadores.size(); i++){
            //por cada array de Escenas del randerizador
            for(int j = 0; j < resultadoRenderizadores.get(i).get().size(); j++){
                //cogemos el tiempo de duracion de cada una de las escenas del array.
                timepoRenderizacion = timepoRenderizacion + resultadoRenderizadores.get(i).get().get(j).getDuracion(); 
            }
  
        }
        
        
        System.out.println("El tiempo total de renderizacion es de : "+timepoRenderizacion);
        
        //Por cada renderizador
        for(int i = 0; i < resultadoRenderizadores.size(); i++){
            //por cada array de Escenas del randerizador
            for(int j = 0; j < resultadoRenderizadores.get(i).get().size(); j++){
                //mostramos la info de cada escena renderizada
                System.out.println("MOSTRAR INFO : " + resultadoRenderizadores.get(i).get().get(j).toString());
                
            }
  
        } 

        
        
            
        

        
        
        
        
    }

    
    
}
