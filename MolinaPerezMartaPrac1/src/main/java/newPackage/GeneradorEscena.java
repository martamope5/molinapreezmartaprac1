/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newPackage;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import static newPackage.Constantes.MIN_NUM_ESCENAS;
import static newPackage.Constantes.MIN_TIEMPO_GENERACION;
import static newPackage.Constantes.VARIACION_NUM_ESCENAS;
import static newPackage.Constantes.VARIACION_TIEMPO;
import static newPackage.Constantes.barrera;
import static newPackage.Constantes.emptySemAltaPrioridad;
import static newPackage.Constantes.emptySemBajaPrioridad;
import static newPackage.Constantes.fillSemAltaPrioridad;
import static newPackage.Constantes.fillSemBajaPrioridad;
import static newPackage.Constantes.listaAltaPrioridad;
import static newPackage.Constantes.listaBajaPrioridad;
import static newPackage.Constantes.mutexAltaPrioridad;
import static newPackage.Constantes.mutexBajaPrioridad;

/**
 *
 * @author martamolinaperez
 */
public class GeneradorEscena implements Callable{
    
    
    private String id;
    Finalizador finRenderizacion;
    
    Random aleatorio = new Random();

    public GeneradorEscena(Finalizador finRend) {
        UUID _id = UUID.randomUUID();
        String variable = "";
        this.id = variable+_id;
        this.finRenderizacion = finRend;
    }

    @Override
    public ArrayList<Escena> call() throws Exception{
       System.out.println("Generando Escenas" );
       ArrayList<Escena> e= ejecucion();
       System.out.println("Fin generacion escenas" );
       finRenderizacion.ejecucion();
       return e;
    }
    
    
   
    public ArrayList<Escena> ejecucion() throws Exception{
                

        ArrayList<Escena> resultado = new ArrayList<>();
        int a = aleatorio.nextInt(VARIACION_NUM_ESCENAS);
        int numEscenasGenerar = a+MIN_NUM_ESCENAS;
        
        int generadas = 0;
        
        while(generadas<numEscenasGenerar){
            a = aleatorio.nextInt(VARIACION_TIEMPO);
            int tiempoGeneracionEscena = MIN_TIEMPO_GENERACION+a;
            Thread.sleep(tiempoGeneracionEscena);
            Escena e = new Escena();
            e.inicializarEscena();
            resultado.add(e);
            
            if(e.isPrioridad()){
                //es una escena de alta prioridad
                fillSemAltaPrioridad.acquire();
                mutexAltaPrioridad.acquire();
                listaAltaPrioridad.add(e);
                mutexAltaPrioridad.release();
                emptySemAltaPrioridad.release();
            }else{
                //es una escena con baja prioridad
                fillSemBajaPrioridad.acquire();
                mutexBajaPrioridad.acquire();
                listaBajaPrioridad.add(e);
                mutexBajaPrioridad.release();
                emptySemBajaPrioridad.release();
            }
            generadas++;
        }
    barrera.release();
    barrera.acquire();
    
    return resultado;
    }
    
}
