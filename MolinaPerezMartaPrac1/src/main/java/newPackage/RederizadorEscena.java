/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newPackage;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import static newPackage.Constantes.emptySemAltaPrioridad;
import static newPackage.Constantes.emptySemBajaPrioridad;
import static newPackage.Constantes.fillSemAltaPrioridad;
import static newPackage.Constantes.fillSemBajaPrioridad;
import static newPackage.Constantes.listaAltaPrioridad;
import static newPackage.Constantes.listaBajaPrioridad;
import static newPackage.Constantes.mainMutex;
import static newPackage.Constantes.mutexAltaPrioridad;
import static newPackage.Constantes.mutexBajaPrioridad;

/**
 *
 * @author martamolinaperez
 */
public class RederizadorEscena implements Callable{
    public static final int RETARDO =10;
    private String id;
    private boolean terminadosGenereadores;
    int idNum;

    public RederizadorEscena() {
        UUID _id = UUID.randomUUID();
        String variable = "";
        this.id = variable+_id;
    }

    
    
    
    
    @Override
    public ArrayList<Escena> call() throws InterruptedException{
        System.out.println("Renderizando Escena" );

        
        Escena e = new Escena();
        boolean tengoEscena;
        ArrayList<Escena> resultado=new ArrayList<>();
        while(true){
            int contAlta =listaAltaPrioridad.size();
            int contBaja = listaBajaPrioridad.size();
                    
            tengoEscena=false;
            mainMutex.acquire();
            if(!listaAltaPrioridad.isEmpty()){
            emptySemAltaPrioridad.acquire();
            mutexAltaPrioridad.acquire();
            
            e = listaAltaPrioridad.get(contAlta);
            
            mutexAltaPrioridad.release();
            fillSemAltaPrioridad.release();
            
            contAlta--;
            
            tengoEscena=true;
            }
            else{
                if(!listaBajaPrioridad.isEmpty()){
                
                    emptySemBajaPrioridad.acquire();
                    mutexBajaPrioridad.acquire();
                    
                    e = listaBajaPrioridad.get(contBaja);
                    
                    mutexBajaPrioridad.release();
                    fillSemBajaPrioridad.release();

                    contBaja--;
                    
                    tengoEscena = true;
                }  
                else{
                    if(terminadosGenereadores){
                        
                        mainMutex.release();
                        return resultado;
                        
                    }
                }
            }
            mainMutex.release();
            
            if(tengoEscena){
                
                Calendar c = Calendar.getInstance();
                e.setHoraIni(c.get(Calendar.HOUR_OF_DAY));
                e.setMinutosIni(c.get(Calendar.MINUTE));
                e.setSegundosIni(c.get(Calendar.SECOND));
                
                TimeUnit.SECONDS.sleep(e.getDuracion());

               
                
                e.setHoraFin(c.get(Calendar.HOUR_OF_DAY));
                e.setMinutosFin(c.get(Calendar.MINUTE));
                e.setSegundosFin(c.get(Calendar.SECOND));
                
                resultado.add(e);
                
            }
            else{
                TimeUnit.SECONDS.sleep(RETARDO);
            }
            
        }
        
    }
    
    
    public void setTerminadosGeneradores(){
        terminadosGenereadores = true;
    }
}
