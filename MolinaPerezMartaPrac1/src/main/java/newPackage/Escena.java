/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newPackage;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;
import java.util.UUID;
import static newPackage.Constantes.MIN_NUM_FOTOGRAMAS;
import static newPackage.Constantes.TIEMPO_DURACION_ESCENA;
import static newPackage.Constantes.VARIACION_NUM_FOTOGRAMAS;

/**
 *
 * @author martamolinaperez
 */
public class Escena {
    
    
    private String id;
    private boolean prioridad;
    private int duracion;
    private int horaGenerado, minutosGenerado, segundosGenerado;
    private int horaIni, minutosIni, segundosIni;
    private int horaFin, minutosFin, segundosFin;
    ArrayList<Fotograma> listaFotogramas;
    
    Random aleatorio = new Random();
    
    Calendar calendario;

    public Escena() {
        
        UUID _id = UUID.randomUUID();
        String variable = "";
        this.id = variable+_id;
        this.prioridad = aleatorio.nextBoolean();
        this.duracion = TIEMPO_DURACION_ESCENA;
        this.listaFotogramas = new ArrayList<>();
        //System.out.println("crea la escena "+id);
        
    }

    public int getDuracion() {
        return duracion;
    }

    
    
    public String getId() {
        return id;
    }

    public boolean isPrioridad() {
        return prioridad;
    }

    public void setHoraGenerado(int horaGenerado) {
        this.horaGenerado = horaGenerado;
    }

    public void setMinutosGenerado(int minutosGenerado) {
        this.minutosGenerado = minutosGenerado;
    }

    public void setSegundosGenerado(int segundosGenerado) {
        this.segundosGenerado = segundosGenerado;
    }

    public void setHoraIni(int horaIni) {
        this.horaIni = horaIni;
    }

    public void setMinutosIni(int minutosIni) {
        this.minutosIni = minutosIni;
    }

    public void setSegundosIni(int segundosIni) {
        this.segundosIni = segundosIni;
    }

    public void setHoraFin(int horaFin) {
        this.horaFin = horaFin;
    }

    public void setMinutosFin(int minutosFin) {
        this.minutosFin = minutosFin;
    }

    public void setSegundosFin(int segundosFin) {
        this.segundosFin = segundosFin;
    }
    
    
    
    public void inicializarEscena(){
        //OBTENEMOS LA HORA DE GENERACION
        calendario = Calendar.getInstance();
        horaGenerado=Calendar.HOUR_OF_DAY;
        minutosGenerado =Calendar.MINUTE;
        segundosGenerado = Calendar.SECOND;
        
        int a = aleatorio.nextInt(VARIACION_NUM_FOTOGRAMAS);
        int numFotogramasAGenerar = a + MIN_NUM_FOTOGRAMAS;
        
        int generados = 0;
                
        while(generados < numFotogramasAGenerar){
            
            Fotograma f = new Fotograma();
            listaFotogramas.add(f);
            duracion = duracion + f.getDuracion();
            generados++;
            
        }
    }

    @Override
    public String toString() {
        return "Escena{" + "id=" + id + 
                ", prioridad=" + prioridad 
                + ", duracion=" + duracion + 
                ", horaGenerado=" + horaGenerado + 
                ", minutosGenerado=" + minutosGenerado + 
                ", segundosGenerado=" + segundosGenerado + 
                ", horaIni=" + horaIni + 
                ", minutosIni=" + minutosIni + 
                ", segundosIni=" + segundosIni + 
                ", horaFin=" + horaFin + 
                ", minutosFin=" + minutosFin + 
                ", segundosFin=" + segundosFin + '}';
    }
    
    
    
    
    
    
    
    
}
