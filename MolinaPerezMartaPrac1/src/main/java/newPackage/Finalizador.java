/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newPackage;

import java.util.ArrayList;

/**
 *
 * @author martamolinaperez
 */
public class Finalizador {
    private ArrayList<RederizadorEscena> listaRenderizadores = new ArrayList<>();

    public Finalizador(ArrayList<RederizadorEscena> listaRenderizadores) {
        this.listaRenderizadores = listaRenderizadores;
    }
    
    
    
    public void ejecucion(){
        for( int i = 0; i<listaRenderizadores.size();i++){
            listaRenderizadores.get(i).setTerminadosGeneradores();
        }
    }
}
