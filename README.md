PRÁCTICA 1
==========
A continuación se presenta la solución teórica a la práctica primera de la 
asignatura. Diferenciando dos partes : 

A N Á L I S I S
----------------
En esta primera parte se especifican la estructuras de datos, variables 
compartidas , semáforos y procedimientos necesarios para la realización de la
práctica. (Según mi criterio).

Estructuras de datos
--------------------
Tipos de datos:

-TDA Fotograma :

    -idFotograma : entero
    -tiempo-fotograma:entero (numero aleatorio entre cotas)
  
  
-TDA listaFotogramas --> Lista[Fotograma] :
  
  
-TDA Escena : 

    -idEscena: entero
    -prioridad : boolean // 0, baja; 1, alta.
    -fotogramasEscena : listaFotograma
    -tiempo : entero
    -horaGenerada : long
    -horaInicioRend : long
    -horaFinRend : long
    -OPERACIONES:
  
        -getter and setters.
    
        -calcularTiempoRend() : horaFinRend-horaInicioRend;
    
    
 
-TDA listaEscenas -->Lista[Escena]


Variables compartidas
---------------------
    listaEscenas lista1 --> Contendrá las escenas de mayor prioridad.
    listaEscenas lista2 --> Contendrá las escenas de menor prioridad.
    tiempoRenderizacion : long

Procesos de apoyo
-----------------
Para el proceso Generador, utilizaremos los siguientes proccesos de apoyo :

    -construirEscena : Creará una nueva escena cada vez que se le invoque, con su
    propio id.
 
 
Para el proceso Renderizador : 

    -escogerEscena : permite al proceso escoger una escena a partir de las listas 
    compartidas, lista1 y lista2, buscando primero si hay alguna con prioridad 
    ALTA (almacenadas en lista1).
 
    -Renderizar(Escena &e): Comprueba si todos los fotogramas han sido procesados, a partir 
    del tiempo de duración.
 
    -comprobarTiempoFotogramas(Escena e) : devuelve si el tiempo transcurrido es igual a la suma de los tiempos de los fotogramas.
 
Para el proceso principal:

    -crearGeneradorEscenas() : genera un nuevo generador de escenas, 
    en el cual alojará un número finito de escenas calculadas mediante un aleatorio,
    teniendo en cuenta las cotas del mismo.
 
    -crearRenderizador() : genera un Renderizador de escenas.
 
    -ejecucion() : comienza aquí la ejecución de ambos procesos.
 
    -noFin() : nos ayuda a controlar la condición de parada de ejecución, 
    en este caso, si existen escenas que renderizar y han finalizado los 
    generadores de escenas.
 
    -finalizar() : finaliza los procesos de los renderizadores.
 
    -mostrarInformacion(): muestra por pantalla la informacion que se nos especifica en el guión de la práctica.
 
Semáforos
---------

 Para la realización de la práctica será necesaria la utilización de los siguientes semáforos:
 
  -exmGeneradores : Semáforo de exclusión mutua entre los generadores de escenas
  para la creación de escenas y puesta a disposición de los renderizadores. 
  Se inicializa a 1.
  
  -exmGR : semáforo de exclusión mutua que será utilizado para controlar el 
  acceso a las listas compartidas entre los generadores y los renderizadores,
  donde los generadores de escenas escriben y los renderizadores leen. También
  se inicializa a 1.
  
  -disponibles1 : Representa la cantidad de espacios disponibles en la lista 
  de escenas de prioridad alta. Se inicializa a la constante MAX_ESCENAS_EN_ESPERA.

  
  -disponibles2 : Idéntica a la anterior, pero representa a la lista de escenas
  con prioridad baja. Se inicializa a la constante MAX_ESCENAS_EN_ESPERA.
  
  -empezarRenderizacion : Semáforode sincronización entre los procesos
  Generador y Renderizador. Se inicializa a 0.
  
  -exmRenderizadores : Semáforo que nos permite controlar la sección crítica de 
  escribir en la lista de escenas resultado a partir de los 
  distintos renderizadores.



D I S E Ñ O
-----------

A continuación, se presenta la parte del diseño de la práctica mediante pseudocódigo.

En la ejecución, se pueden distinguir dos procesos claros, a parte del proceso general :

**Proceso 1 : Generador de escenas :**
    
```
Run {
	Prioridad : boolean;
	Int contadorEscenas;

    Do{
    exmGernerador.wait();
	escena = construirEscena();
	contadorEscenas++;
	exmGR.wait();

	if(escena.getPrioridad==0)
	{
		lista1.push(Escena);
		disponibles1.wait();
	}
	
	else
	{
		lista2.push(Escena);
		disponibles2.wait();
	}
	exmGR.signal();

    exmGenerador.signal();

    empezarRenderizacion.signal();


    }while(contadorEscenas<this.num_escenas);

}

Escena construirEscena(){
    //ramdon para prioridad
    Int prioridad= generarAleatorio(1);
    //random para numero de fotogramas
    Int aleatorio= generarAleatorio(variacion_num_fotogramas);
    numFotogramas = min_num_fotogramas + aleatorio;
    //llamada al constructor de escena 
    Escena e= new Escena(prioridad, aleatorio );
}
```



**Proceso 2 : Renderizador de escenas :**
    
```
void run{
    long inicio = getTime();
    while(lista1.size()>0 || lista2.size()>0)
    {
        empezarRanderizacion.wait();
        exmGR.wait();
	    Escena = escogerEscena();
	    Escena.setHoraInicioRend(getTime());
        exmGR.signal();

        renderizar(&Escena);

        exmRenderizador.wait();
        Escena.setHoraFinRend(getTime());
	    ListaResultado.push(Escena);
        exmRenderizador.signal();
    }
    long fin=getTime();
    tiempoRenderizacion = fin-inicio;
}




Escena escogerEscena(){
    Escena e;
    
    if(lista1.size>0){
	    e = Lista1.pop();
	    Disponibles1.signal();
    }
    Else{
	    if(lista2.size>0){
	        e = Lista2.pop();
	        Disponibles2.signal();
	    }
    }
Return e;

}


Void Renderizar(Escena& e){
    Boolean seguir = true;
    while(seguir){
        Seguir = comprobarTiempoFotogramas(e);//devuelve si el tiempo transcurrido es igual a la suma de los tiempos de los fotogramas.
    }
}
```






**HILO PRINCIPAL :**
    
```
ArrayList<Generadores> generadores;
ArrayList<Renderizadores> renderizadores;

//Crear GeneradoresEscenas
for(int i =0;i<NUM_GENERADORES ;i++){

	creaGenerador();
}

for(int i =0;i<NUM_RANDERIZACORES ;i++){

	creaRenderizador();
}

while(noFin()){
	ejecucion();//comienza la ejecución concurrente de los generadores de escenas y lo renderizadores.
}

Finalizar();

MostrarInformacion();



Boolean noFin(){
//comprueba que los Generadores de escenas hayan terminado su ejecución, que si están las listas a 0, significará que sí .
//comprueba que las listas están vacías.
Boolean fin=false;
if(lista1.isEmpty() && lista2.isEmpty()){
    fin=true;
}
Return fin;
}



void mostrarInformacion (){
    mostrarTamañoListaResultado(); //nos mostrara por pantalla el numero de escenas resultado.
    mostrarTiempoRenderizacion(); //nos muestra por pantalla el valor de la variable compartida.
    for(i =0;i<ListaResultado.size/(; i++){
        ListaResultado[i].mostrarHoraGenerada();
        ListaResultado[i].mostrarHoraInicioRend();
        for(j=0;j<ListaResultado[i].fotogramasEscena.size();j++){
            ListaResultado[i].fotogramasEscena[j].mostrarTiempoFotograma();
        }
        ListaResultado[i].mostrarHoraFinRend();
        ListaResultado[i].mostrarTiempoTotal();
    }
    
}

```









